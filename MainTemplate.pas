unit MainTemplate;

{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.05.2012                                }
{                                                       }
{     ������ : 1.0.4521                                }
{                                                       }
{*******************************************************}
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, cxCheckBox, WebDisp, SOAPConn, InvokeRegistry, Rio, SOAPHTTPClient, WSDLIntf, SOAPPasInv,
  SOAPHTTPPasInv, TypInfo, WebServExp, WSDLBind, XMLSchema, WSDLPub, xmldom,
  XMLIntf, msxmldom, XMLDoc, FIBDataSet, pFIBDataSet, Provider, Xmlxform, WinInet,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    aLokacija: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    aStaratel: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aPocinati: TAction;
    aKartica: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aWebService: TAction;
    XMLDocument1: TXMLDocument;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aLokacijaExecute(Sender: TObject);
    procedure aStaratelExecute(Sender: TObject);
    procedure aPocinatiExecute(Sender: TObject);
    procedure aKarticaExecute(Sender: TObject);
    procedure aWebServiceExecute(Sender: TObject);
    function IsInternetConnected: boolean;
  private
    { Private declarations }
  public
    { Public declarations }
    poraka : boolean;
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, dmUnit, Lokacija, MK, Staratel,
  Pocinati, Kartica, partnerFromFak, KomSvc, karticaPartner, cxConstantsMak;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aKarticaExecute(Sender: TObject);
begin
     frmKartica:=TfrmKartica.Create(Application);
     frmKartica.ShowModal;
     frmKartica.free;
end;

procedure TfrmMain.aLokacijaExecute(Sender: TObject);
begin
     frmLokacija:=TfrmLokacija.Create(Application);
     frmLokacija.ShowModal;
     frmLokacija.free;
end;

procedure TfrmMain.aPocinatiExecute(Sender: TObject);
begin
     frmPocinati:=TfrmPocinati.Create(Application);
     frmPocinati.ShowModal;
     frmPocinati.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aStaratelExecute(Sender: TObject);
begin
     frmStaratel:=TfrmStaratel.Create(Application);
     frmStaratel.ShowModal;
     frmStaratel.free;
end;

procedure TfrmMain.aWebServiceExecute(Sender: TObject);
var
  xmlString : Widestring;
  dataset : IXMLRootType;
  i : Integer;
  status : TStatusWindowHandle;
  //s_data : string;
  faktura:string;
  lokacija:Integer;
begin
  if (IsInternetConnected = true) then
  begin
    if (poraka) then status := cxCreateStatusWindow();
    try
      //s_data := KursDatum.EditValue;

      faktura:='1';
      lokacija:=1;
      //xmlString := GetKomSvcSoap.GET_PARTNER_FROM_FAK(faktura,lokacija);
      xmlString := GetKomSvcSoap.GET_KARTICA_PARTNER(5, 1, 1);
      XMLDocument1.LoadFromXML(xmlString);
      XMLDocument1.SaveToFile('C:\cxCache\test.xml');

      dataset := karticaPartner.GetRoot(XMLDocument1);
      //GetRoot(XMLDocument1);


    //  ShowMessage(InttoStr(dataset.PARTNER.FLAG));

//      cxGrid1TableView1.DataController.MultiSelect := true;
//      cxGrid1TableView1.Controller.SelectAllRecords;
//      cxGrid1TableView1.Controller.DeleteSelection;
//      cxGrid1TableView1.DataController.MultiSelect := false;

      ShowMessage(inttostr(dataset.Count));

      for I := 0 to dataset.Count - 1 do
      begin

      ShowMessage(inttoStr(dataset.KARTICAPARTNER[i].DOLZI));
//        cxGrid1TableView1.Controller.CreateNewRecord(true);
//
//        Oznaka.EditValue := dataset.KursZbir[i].Oznaka;
//        Sreden.EditValue := dataset.KursZbir[i].Sreden;
//        Datum.EditValue := dataset.KursZbir[i].Datum;
//        Drzava.EditValue := dataset.KursZbir[i].Drzava;
      end;
//
//      lblSredenKurs.Caption := '������ ���� �� ���� �� ����� : ' + s_data;
    finally
      if (poraka) then cxRemoveStatusWindow(status);
      //cxGrid1TableView1.Controller.FocusedRowIndex := 0;
    end;
  end;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin

   dmKon.fibPateka.Connected := true;
   dmkon.aplikacija:='GIS';

    firma:=dmKon.re;

    if (dmKon.fibBaza.Connected=false) then
    begin
      Application.Terminate;
    end
    else
    begin
         otvoriTabeli;
         Application.HelpFile := ExtractFilePath(Application.ExeName) + 'HRM_Help.chm';  //pateka do help -fajlot
    end;
    dmRes.SkinLista(cxBarSkin);
    dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  cxSetEureka('danica.stojkova@codex.mk');

  frmMK := TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.Free;
end;

procedure TfrmMain.OtvoriTabeli;
begin
  // ������ �� ���������� dataset-���

end;

function TfrmMain.IsInternetConnected: boolean;
var
  dw_ConnectionTypes : DWORD;
begin
    dw_ConnectionTypes:=
    INTERNET_CONNECTION_LAN +
    INTERNET_CONNECTION_MODEM +
    INTERNET_CONNECTION_PROXY;
    Result:=InternetGetConnectedState(@dw_ConnectionTypes,0);
end;
end.
