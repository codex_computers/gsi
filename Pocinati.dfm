inherited frmPocinati: TfrmPocinati
  Caption = #1055#1086#1095#1080#1085#1072#1090#1080
  ClientHeight = 655
  ClientWidth = 810
  ExplicitWidth = 826
  ExplicitHeight = 693
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 810
    Height = 194
    ExplicitWidth = 810
    ExplicitHeight = 194
    inherited cxGrid1: TcxGrid
      Width = 806
      Height = 190
      ExplicitWidth = 806
      ExplicitHeight = 190
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPocinati
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1076#1072#1090#1086#1094#1080
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1BLOK: TcxGridDBColumn
          DataBinding.FieldName = 'BLOK'
          Width = 60
        end
        object cxGrid1DBTableView1RED: TcxGridDBColumn
          DataBinding.FieldName = 'RED'
          Width = 49
        end
        object cxGrid1DBTableView1GROB: TcxGridDBColumn
          DataBinding.FieldName = 'GROB'
          Width = 69
        end
        object cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME_IME'
          Width = 162
        end
        object cxGrid1DBTableView1VOZRAST: TcxGridDBColumn
          DataBinding.FieldName = 'VOZRAST'
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_POCINAT: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_POCINAT'
          Width = 115
        end
        object cxGrid1DBTableView1DATUM_ZAKOPAN: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_ZAKOPAN'
          Width = 120
        end
        object cxGrid1DBTableView1DATUM_NAPLATA: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_NAPLATA'
          Width = 101
        end
        object cxGrid1DBTableView1INFO_ISPRATNICA: TcxGridDBColumn
          DataBinding.FieldName = 'INFO_ISPRATNICA'
          Width = 125
        end
        object cxGrid1DBTableView1NAPLATA_IZNOS: TcxGridDBColumn
          Caption = #1053#1072#1087#1083#1072#1090'e'#1085#1086' - '#1080#1079#1085#1086#1089
          DataBinding.FieldName = 'NAPLATA_IZNOS'
          Width = 100
        end
        object cxGrid1DBTableView1BR_RESENIE_ZAKOP: TcxGridDBColumn
          DataBinding.FieldName = 'BR_RESENIE_ZAKOP'
          Width = 100
        end
        object cxGrid1DBTableView1LOKACIJA_ID: TcxGridDBColumn
          DataBinding.FieldName = 'LOKACIJA_ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1NAZIVLOKACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIVLOKACIJA'
          Width = 161
        end
        object cxGrid1DBTableView1OdrzuvanjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'OdrzuvanjeNaziv'
          Width = 100
        end
        object cxGrid1DBTableView1PREZIMEIMESTARATEL: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIMEIMESTARATEL'
          Width = 250
        end
        object cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'STARATEL_SIFRA'
          Width = 105
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_ULICA'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_BROJ'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_MESTO'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 320
    Width = 810
    Height = 312
    ExplicitTop = 320
    ExplicitWidth = 810
    ExplicitHeight = 312
    inherited Label1: TLabel
      Left = 581
      Top = 9
      Visible = False
      ExplicitLeft = 581
      ExplicitTop = 9
    end
    object Label9: TLabel [1]
      Left = 35
      Top = 228
      Width = 100
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1077#1096'. '#1079#1072' '#1079#1072#1082#1086#1087' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel [2]
      Left = 22
      Top = 255
      Width = 113
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1051#1086#1082#1072#1094#1080#1112#1072' '#1085#1072' '#1075#1088#1086#1073' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 637
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPocinati
      TabOrder = 4
      Visible = False
      ExplicitLeft = 637
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 711
      Top = 248
      TabOrder = 8
      ExplicitLeft = 711
      ExplicitTop = 248
    end
    inherited ZapisiButton: TcxButton
      Left = 630
      Top = 248
      TabOrder = 7
      ExplicitLeft = 630
      ExplicitTop = 248
    end
    object gbPocinat: TcxGroupBox
      Left = 14
      Top = 12
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1086#1095#1080#1085#1072#1090
      TabOrder = 0
      Height = 93
      Width = 580
      object Label3: TLabel
        Left = 21
        Top = 60
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1042#1086#1079#1088#1072#1089#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 21
        Top = 33
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object vozrast: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 57
        BeepOnEnter = False
        DataBinding.DataField = 'VOZRAST'
        DataBinding.DataSource = dm.dsPocinati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 74
      end
      object PrezimeIme: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 30
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME_IME'
        DataBinding.DataSource = dm.dsPocinati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 418
      end
    end
    object gbData: TcxGroupBox
      Left = 14
      Top = 111
      Caption = #1044#1072#1090#1072
      TabOrder = 1
      Height = 97
      Width = 290
      object Label7: TLabel
        Left = 21
        Top = 33
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1082#1086#1075#1072' '#1077' '#1087#1086#1095#1080#1085#1072#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 21
        Top = 60
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1082#1086#1075#1072' '#1077' '#1079#1072#1082#1086#1087#1072#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DATUM_POCINAT: TcxDBDateEdit
        Tag = 1
        Left = 127
        Top = 30
        DataBinding.DataField = 'DATUM_POCINAT'
        DataBinding.DataSource = dm.dsPocinati
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object DATUM_ZAKOPAN: TcxDBDateEdit
        Tag = 1
        Left = 127
        Top = 57
        DataBinding.DataField = 'DATUM_ZAKOPAN'
        DataBinding.DataSource = dm.dsPocinati
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object gbNaplateno: TcxGroupBox
      Left = 310
      Top = 111
      Caption = #1053#1072#1087#1083#1072#1090#1077#1085#1086
      TabOrder = 2
      Height = 122
      Width = 284
      object Label5: TLabel
        Left = 21
        Top = 60
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1087#1088#1080#1079'. '#1073#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 38
        Top = 33
        Width = 84
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1076#1072#1090#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 22
        Top = 87
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1080#1079#1085#1086#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object INFO_ISPRATNICA: TcxDBTextEdit
        Tag = 1
        Left = 127
        Top = 57
        BeepOnEnter = False
        DataBinding.DataField = 'INFO_ISPRATNICA'
        DataBinding.DataSource = dm.dsPocinati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 122
      end
      object DATUM_NAPLATA: TcxDBDateEdit
        Tag = 1
        Left = 128
        Top = 30
        DataBinding.DataField = 'DATUM_NAPLATA'
        DataBinding.DataSource = dm.dsPocinati
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object NAPLATA_IZNOS: TcxDBTextEdit
        Left = 128
        Top = 84
        BeepOnEnter = False
        DataBinding.DataField = 'NAPLATA_IZNOS'
        DataBinding.DataSource = dm.dsPocinati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object BR_RESENIE_ZAKOP: TcxDBTextEdit
      Left = 141
      Top = 225
      BeepOnEnter = False
      DataBinding.DataField = 'BR_RESENIE_ZAKOP'
      DataBinding.DataSource = dm.dsPocinati
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 122
    end
    object cbLokacija: TcxDBLookupComboBox
      Tag = 1
      Left = 192
      Top = 252
      DataBinding.DataField = 'LOKACIJA_ID'
      DataBinding.DataSource = dm.dsPocinati
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1051#1086#1082#1072#1094#1080#1112#1072
          FieldName = 'NAZIVLOKACIJA'
        end
        item
          FieldName = 'BLOK'
        end
        item
          FieldName = 'RED'
        end
        item
          FieldName = 'GROB'
        end>
      Properties.ListSource = dm.dsLokacija
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 367
    end
    object txtlokacija: TcxDBTextEdit
      Tag = 1
      Left = 141
      Top = 252
      BeepOnEnter = False
      DataBinding.DataField = 'LOKACIJA_ID'
      DataBinding.DataSource = dm.dsPocinati
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 50
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 810
    ExplicitWidth = 810
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 632
    Width = 810
    ExplicitTop = 632
    ExplicitWidth = 810
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 208
  end
  inherited dxBarManager1: TdxBarManager
    Left = 448
    Top = 224
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 102
      FloatClientHeight = 76
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Top = 208
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40820.553096932870000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
