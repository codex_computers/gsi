inherited frmStaratel: TfrmStaratel
  Caption = #1057#1090#1072#1088#1072#1090#1077#1083#1080
  ClientHeight = 687
  ClientWidth = 798
  ExplicitWidth = 814
  ExplicitHeight = 725
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 798
    Height = 258
    ExplicitWidth = 798
    ExplicitHeight = 258
    inherited cxGrid1: TcxGrid
      Width = 794
      Height = 254
      ExplicitWidth = 794
      ExplicitHeight = 254
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsStaratel
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1076#1072#1090#1086#1094#1080
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 65
        end
        object cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME_IME'
          Width = 194
        end
        object cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_ULICA'
          Width = 134
        end
        object cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_BROJ'
          Width = 76
        end
        object cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA_MESTO'
          Width = 202
        end
        object cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'STARATEL_SIFRA'
          Width = 104
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 384
    Width = 798
    Height = 280
    ExplicitTop = 384
    ExplicitWidth = 798
    ExplicitHeight = 280
    inherited Label1: TLabel
      Left = 541
      Visible = False
      ExplicitLeft = 541
    end
    object Label7: TLabel [1]
      Left = 69
      Top = 28
      Width = 84
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 597
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsStaratel
      TabOrder = 2
      Visible = False
      ExplicitLeft = 597
    end
    inherited OtkaziButton: TcxButton
      Left = 707
      Top = 240
      TabOrder = 4
      ExplicitLeft = 707
      ExplicitTop = 240
    end
    inherited ZapisiButton: TcxButton
      Left = 626
      Top = 240
      TabOrder = 3
      ExplicitLeft = 626
      ExplicitTop = 240
    end
    object cxGroupBox1: TcxGroupBox
      Left = 16
      Top = 52
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1089#1090#1072#1088#1072#1090#1077#1083
      TabOrder = 1
      Height = 182
      Width = 582
      object Label5: TLabel
        Left = 37
        Top = 114
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' - '#1084#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 37
        Top = 87
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' - '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 37
        Top = 60
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1088#1077#1089#1072' - '#1091#1083#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 37
        Top = 33
        Width = 100
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 80
        Top = 141
        Width = 57
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1064#1080#1092#1088#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ulica: TcxDBTextEdit
        Tag = 1
        Left = 143
        Top = 57
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA_ULICA'
        DataBinding.DataSource = dm.dsStaratel
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 400
      end
      object mesto: TcxDBTextEdit
        Tag = 1
        Left = 143
        Top = 111
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA_MESTO'
        DataBinding.DataSource = dm.dsStaratel
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 400
      end
      object broj: TcxDBTextEdit
        Tag = 1
        Left = 143
        Top = 84
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA_BROJ'
        DataBinding.DataSource = dm.dsStaratel
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 400
      end
      object PrezimeIme: TcxDBTextEdit
        Tag = 1
        Left = 143
        Top = 30
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME_IME'
        DataBinding.DataSource = dm.dsStaratel
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 400
      end
      object STARATEL_SIFRA: TcxDBTextEdit
        Left = 143
        Top = 138
        BeepOnEnter = False
        DataBinding.DataField = 'STARATEL_SIFRA'
        DataBinding.DataSource = dm.dsStaratel
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 98
      end
    end
    object datumdo: TcxDBDateEdit
      Left = 159
      Top = 25
      DataBinding.DataField = 'DATUM_DO'
      DataBinding.DataSource = dm.dsStaratel
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 121
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 798
    ExplicitWidth = 798
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 664
    Width = 798
    ExplicitTop = 664
    ExplicitWidth = 798
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 272
    Top = 232
  end
  inherited PopupMenu1: TPopupMenu
    Left = 368
    Top = 232
  end
  inherited dxBarManager1: TdxBarManager
    Top = 232
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 102
      FloatClientHeight = 76
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 176
    Top = 248
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40820.495705960650000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
