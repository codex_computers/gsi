program GIS;

uses
  ExceptionLog,
  Forms,
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  Login in '..\Share2010\Login.pas' {frmLogin},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  MainTemplate in 'MainTemplate.pas' {frmMain},
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Master in '..\Share2010\Master.pas' {frmMaster},
  Lokacija in 'Lokacija.pas' {frmLokacija},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  MK in '..\Share2010\MK.pas' {frmMK},
  Staratel in 'Staratel.pas' {frmStaratel},
  Pocinati in 'Pocinati.pas' {frmPocinati},
  Kartica in 'Kartica.pas' {frmKartica},
  Utils in '..\Share2010\Utils.pas',
  FormConfig in '..\Share2010\FormConfig.pas' {frmFormConfig},
  KomSvc in 'KomSvc.pas',
  partnerFromFak in 'partnerFromFak.pas',
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  karticaPartner in 'karticaPartner.pas';

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '���';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='GIS';

  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end
  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.
