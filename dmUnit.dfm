object dm: Tdm
  OldCreateOrder = False
  Height = 369
  Width = 464
  object dsLokacija: TDataSource
    DataSet = tblLokacija
    Left = 96
    Top = 32
  end
  object tblLokacija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE GIS_LOKACIJA'
      'SET '
      '    BLOK = :BLOK,'
      '    RED = :RED,'
      '    GROB = :GROB,'
      '    ODRZUVANJE = :ODRZUVANJE,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    STARATEL_ID = :STARATEL_ID'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    GIS_LOKACIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO GIS_LOKACIJA('
      '    ID,'
      '    BLOK,'
      '    RED,'
      '    GROB,'
      '    ODRZUVANJE,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    STARATEL_ID'
      ')'
      'VALUES('
      '    :ID,'
      '    :BLOK,'
      '    :RED,'
      '    :GROB,'
      '    :ODRZUVANJE,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :STARATEL_ID'
      ')')
    RefreshSQL.Strings = (
      ' select gl.id,'
      '       gl.blok,'
      '       gl.red,'
      '       gl.grob,'
      '       gl.odrzuvanje,'
      '       case  when gl.odrzuvanje = 1 then '#39#1044#1072#39
      '             when gl.odrzuvanje = 0 then '#39#1053#1077#39
      '       end "OdrzuvanjeNaziv",'
      '       gl.ts_ins,'
      '       gl.ts_upd,'
      '       gl.usr_ins,'
      '       gl.usr_upd,'
      
        '       '#39#1041#1083#1086#1082' - '#39' || gl.blok || '#39', '#1056#1077#1076' - '#39'||gl.red||'#39', '#1043#1088#1086#1073' - '#39'||' +
        'gl.grob as NazivLokacija,'
      '       gl.staratel_id,'
      
        '       gs.prezime_ime, gs.adresa_ulica, gs.adresa_broj, gs.adres' +
        'a_mesto, gs.datum_do, gs.staratel_sifra'
      'from gis_lokacija gl'
      'left outer join gis_staratel gs on gs.id = gl.staratel_id'
      ''
      ' WHERE '
      '        GL.ID = :OLD_ID'
      '    '
      'order by gl.blok,gl.red,gl.grob')
    SelectSQL.Strings = (
      ' select gl.id,'
      '       gl.blok,'
      '       gl.red,'
      '       gl.grob,'
      '       gl.odrzuvanje,'
      '       case  when gl.odrzuvanje = 1 then '#39#1044#1072#39
      '             when gl.odrzuvanje = 0 then '#39#1053#1077#39
      '       end "OdrzuvanjeNaziv",'
      '       gl.ts_ins,'
      '       gl.ts_upd,'
      '       gl.usr_ins,'
      '       gl.usr_upd,'
      
        '       '#39#1041#1083#1086#1082' - '#39' || gl.blok || '#39', '#1056#1077#1076' - '#39'||gl.red||'#39', '#1043#1088#1086#1073' - '#39'||' +
        'gl.grob as NazivLokacija,'
      '       gl.staratel_id,'
      
        '       gs.prezime_ime, gs.adresa_ulica, gs.adresa_broj, gs.adres' +
        'a_mesto, gs.datum_do, gs.staratel_sifra'
      'from gis_lokacija gl'
      'left outer join gis_staratel gs on gs.id = gl.staratel_id'
      'order by gl.blok,gl.red,gl.grob')
    AutoUpdateOptions.UpdateTableName = 'GIS_LOKACIJA'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_GIS_LOKACIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 24
    Top = 32
    object tblLokacijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblLokacijaBLOK: TFIBStringField
      DisplayLabel = #1041#1083#1086#1082
      FieldName = 'BLOK'
      Size = 25
      EmptyStrToNull = True
    end
    object tblLokacijaRED: TFIBStringField
      DisplayLabel = #1056#1077#1076
      FieldName = 'RED'
      Size = 25
      EmptyStrToNull = True
    end
    object tblLokacijaGROB: TFIBStringField
      DisplayLabel = #1043#1088#1086#1073
      FieldName = 'GROB'
      Size = 25
      EmptyStrToNull = True
    end
    object tblLokacijaODRZUVANJE: TFIBSmallIntField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' '#1079#1072' '#1086#1076#1088#1078#1091#1074#1072#1114#1077
      FieldName = 'ODRZUVANJE'
    end
    object tblLokacijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblLokacijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblLokacijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblLokacijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblLokacijaOdrzuvanjeNaziv: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' '#1079#1072' '#1086#1076#1088#1078#1091#1074#1072#1114#1077
      FieldName = 'OdrzuvanjeNaziv'
      Size = 2
      EmptyStrToNull = True
    end
    object tblLokacijaNAZIVLOKACIJA: TFIBStringField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072
      FieldName = 'NAZIVLOKACIJA'
      Size = 99
      EmptyStrToNull = True
    end
    object tblLokacijaSTARATEL_ID: TFIBIntegerField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083' ('#1064#1080#1092#1088#1072')'
      FieldName = 'STARATEL_ID'
    end
    object tblLokacijaPREZIME_IME: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083
      FieldName = 'PREZIME_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object tblLokacijaADRESA_ULICA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA_ULICA'
      Size = 50
      EmptyStrToNull = True
    end
    object tblLokacijaADRESA_BROJ: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' -'#1041#1088#1086#1112
      FieldName = 'ADRESA_BROJ'
      Size = 10
      EmptyStrToNull = True
    end
    object tblLokacijaADRESA_MESTO: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' ('#1052#1077#1089#1090#1086')'
      FieldName = 'ADRESA_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblLokacijaDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblLokacijaSTARATEL_SIFRA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'STARATEL_SIFRA'
      EmptyStrToNull = True
    end
  end
  object dsStaratel: TDataSource
    DataSet = tblStaratel
    Left = 96
    Top = 96
  end
  object tblStaratel: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE GIS_STARATEL'
      'SET '
      '    PREZIME_IME = :PREZIME_IME,'
      '    ADRESA_ULICA = :ADRESA_ULICA,'
      '    ADRESA_BROJ = :ADRESA_BROJ,'
      '    ADRESA_MESTO = :ADRESA_MESTO,'
      '    DATUM_DO = :DATUM_DO,'
      '    STARATEL_SIFRA = :STARATEL_SIFRA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    GIS_STARATEL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO GIS_STARATEL('
      '    ID,'
      '    PREZIME_IME,'
      '    ADRESA_ULICA,'
      '    ADRESA_BROJ,'
      '    ADRESA_MESTO,'
      '    DATUM_DO,'
      '    STARATEL_SIFRA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :PREZIME_IME,'
      '    :ADRESA_ULICA,'
      '    :ADRESA_BROJ,'
      '    :ADRESA_MESTO,'
      '    :DATUM_DO,'
      '    :STARATEL_SIFRA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select gs.id,'
      '       gs.prezime_ime,'
      '       gs.adresa_ulica,'
      '       gs.adresa_broj,'
      '       gs.adresa_mesto,'
      '       gs.datum_do,'
      '       gs.STARATEL_SIFRA,'
      '       gs.ts_ins,'
      '       gs.ts_upd,'
      '       gs.usr_ins,'
      '       gs.usr_upd'
      'from gis_staratel gs'
      ''
      ' WHERE '
      '        GS.ID = :OLD_ID'
      '    '
      'order by gs.prezime_ime')
    SelectSQL.Strings = (
      'select gs.id,'
      '       gs.prezime_ime,'
      '       gs.adresa_ulica,'
      '       gs.adresa_broj,'
      '       gs.adresa_mesto,'
      '       gs.datum_do,'
      '       gs.STARATEL_SIFRA,'
      '       gs.ts_ins,'
      '       gs.ts_upd,'
      '       gs.usr_ins,'
      '       gs.usr_upd'
      'from gis_staratel gs'
      'order by gs.prezime_ime')
    AutoUpdateOptions.UpdateTableName = 'GIS_STARATEL'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_GIS_STARATEL_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 24
    Top = 96
    object tblStaratelID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStaratelPREZIME_IME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077
      FieldName = 'PREZIME_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelADRESA_ULICA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1091#1083#1080#1094#1072
      FieldName = 'ADRESA_ULICA'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelADRESA_BROJ: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1073#1088#1086#1112
      FieldName = 'ADRESA_BROJ'
      Size = 10
      EmptyStrToNull = True
    end
    object tblStaratelADRESA_MESTO: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1084#1077#1089#1090#1086
      FieldName = 'ADRESA_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_INS'
    end
    object tblStaratelTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'TS_UPD'
    end
    object tblStaratelUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' - '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblStaratelSTARATEL_SIFRA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'STARATEL_SIFRA'
      EmptyStrToNull = True
    end
  end
  object dsPocinati: TDataSource
    DataSet = tblPocinati
    Left = 96
    Top = 152
  end
  object tblPocinati: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE GIS_POCINATI'
      'SET '
      '    PREZIME_IME = :PREZIME_IME,'
      '    VOZRAST = :VOZRAST,'
      '    DATUM_POCINAT = :DATUM_POCINAT,'
      '    DATUM_ZAKOPAN = :DATUM_ZAKOPAN,'
      '    DATUM_NAPLATA = :DATUM_NAPLATA,'
      '    INFO_ISPRATNICA = :INFO_ISPRATNICA,'
      '    BR_RESENIE_ZAKOP = :BR_RESENIE_ZAKOP,'
      '    NAPLATA_IZNOS = :NAPLATA_IZNOS,'
      '    LOKACIJA_ID = :LOKACIJA_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    GIS_POCINATI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO GIS_POCINATI('
      '    ID,'
      '    PREZIME_IME,'
      '    VOZRAST,'
      '    DATUM_POCINAT,'
      '    DATUM_ZAKOPAN,'
      '    DATUM_NAPLATA,'
      '    INFO_ISPRATNICA,'
      '    BR_RESENIE_ZAKOP,'
      '    NAPLATA_IZNOS,'
      '    LOKACIJA_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :PREZIME_IME,'
      '    :VOZRAST,'
      '    :DATUM_POCINAT,'
      '    :DATUM_ZAKOPAN,'
      '    :DATUM_NAPLATA,'
      '    :INFO_ISPRATNICA,'
      '    :BR_RESENIE_ZAKOP,'
      '    :NAPLATA_IZNOS,'
      '    :LOKACIJA_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      '        select gp.id,'
      '       gp.prezime_ime,'
      '       gp.vozrast,'
      '       gp.datum_pocinat,'
      '       gp.datum_zakopan,'
      '       gp.datum_naplata,'
      '       gp.info_ispratnica,'
      '       gp.br_resenie_zakop,'
      '       gp.naplata_iznos,'
      '       gp.lokacija_id,'
      '       gl.blok,'
      '       gl.red,'
      '       gl.grob,'
      '       gl.odrzuvanje,'
      '       case  when gl.odrzuvanje = 1 then '#39#1044#1072#39
      '             when gl.odrzuvanje = 0 then '#39#1053#1077#39
      '       end "OdrzuvanjeNaziv",'
      
        '       gs.prezime_ime as prezimeImeStaratel, gs.adresa_ulica, gs' +
        '.adresa_broj, gs.adresa_mesto, gs.datum_do, gs.staratel_sifra,'
      
        '       '#39#1041#1083#1086#1082' - '#39' || gl.blok || '#39', '#1056#1077#1076' - '#39'||gl.red||'#39', '#1043#1088#1086#1073' - '#39'||' +
        'gl.grob as NazivLokacija,'
      '       gp.ts_ins,'
      '       gp.ts_upd,'
      '       gp.usr_ins,'
      '       gp.usr_upd'
      'from gis_pocinati gp'
      'left outer join gis_lokacija gl on gl.id = gp.lokacija_id'
      'left outer join gis_staratel gs on gs.id = gl.staratel_id'
      ''
      ' WHERE '
      '        GP.ID = :OLD_ID'
      '    '
      'order by gl.blok,gl.red,gl.grob')
    SelectSQL.Strings = (
      '        select gp.id,'
      '       gp.prezime_ime,'
      '       gp.vozrast,'
      '       gp.datum_pocinat,'
      '       gp.datum_zakopan,'
      '       gp.datum_naplata,'
      '       gp.info_ispratnica,'
      '       gp.br_resenie_zakop,'
      '       gp.naplata_iznos,'
      '       gp.lokacija_id,'
      '       gl.blok,'
      '       gl.red,'
      '       gl.grob,'
      '       gl.odrzuvanje,'
      '       case  when gl.odrzuvanje = 1 then '#39#1044#1072#39
      '             when gl.odrzuvanje = 0 then '#39#1053#1077#39
      '       end "OdrzuvanjeNaziv",'
      
        '       gs.prezime_ime as prezimeImeStaratel, gs.adresa_ulica, gs' +
        '.adresa_broj, gs.adresa_mesto, gs.datum_do, gs.staratel_sifra,'
      
        '       '#39#1041#1083#1086#1082' - '#39' || gl.blok || '#39', '#1056#1077#1076' - '#39'||gl.red||'#39', '#1043#1088#1086#1073' - '#39'||' +
        'gl.grob as NazivLokacija,'
      '       gp.ts_ins,'
      '       gp.ts_upd,'
      '       gp.usr_ins,'
      '       gp.usr_upd'
      'from gis_pocinati gp'
      'left outer join gis_lokacija gl on gl.id = gp.lokacija_id'
      'left outer join gis_staratel gs on gs.id = gl.staratel_id'
      'order by gl.blok,gl.red,gl.grob')
    AutoUpdateOptions.UpdateTableName = 'GIS_POCINATI'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_GIS_POCINATI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 24
    Top = 152
    object tblPocinatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPocinatiPREZIME_IME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1087#1086#1095#1080#1085#1072#1090#1080#1086#1090
      FieldName = 'PREZIME_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiVOZRAST: TFIBSmallIntField
      DisplayLabel = #1042#1086#1079#1088#1072#1089#1090
      FieldName = 'VOZRAST'
    end
    object tblPocinatiDATUM_POCINAT: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' - '#1082#1086#1075#1072' '#1077' '#1087#1086#1095#1080#1085#1072#1090
      FieldName = 'DATUM_POCINAT'
    end
    object tblPocinatiDATUM_ZAKOPAN: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' - '#1082#1086#1075#1072' '#1077' '#1079#1072#1082#1086#1087#1072#1085
      FieldName = 'DATUM_ZAKOPAN'
    end
    object tblPocinatiDATUM_NAPLATA: TFIBDateField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1077#1085#1086' - '#1076#1072#1090#1072
      FieldName = 'DATUM_NAPLATA'
    end
    object tblPocinatiINFO_ISPRATNICA: TFIBStringField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1077#1085#1086' - '#1087#1088#1080#1079'. '#1073#1088'.'
      FieldName = 'INFO_ISPRATNICA'
      Size = 25
      EmptyStrToNull = True
    end
    object tblPocinatiBR_RESENIE_ZAKOP: TFIBStringField
      DisplayLabel = #1056#1077#1096'. '#1079#1072' '#1079#1072#1082#1086#1087
      FieldName = 'BR_RESENIE_ZAKOP'
      EmptyStrToNull = True
    end
    object tblPocinatiLOKACIJA_ID: TFIBIntegerField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'LOKACIJA_ID'
    end
    object tblPocinatiBLOK: TFIBStringField
      DisplayLabel = #1041#1083#1086#1082
      FieldName = 'BLOK'
      Size = 25
      EmptyStrToNull = True
    end
    object tblPocinatiRED: TFIBStringField
      DisplayLabel = #1056#1077#1076
      FieldName = 'RED'
      Size = 25
      EmptyStrToNull = True
    end
    object tblPocinatiGROB: TFIBStringField
      DisplayLabel = #1043#1088#1086#1073
      FieldName = 'GROB'
      Size = 25
      EmptyStrToNull = True
    end
    object tblPocinatiODRZUVANJE: TFIBSmallIntField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' '#1086#1076#1088#1078#1091#1074#1072#1114#1077
      FieldName = 'ODRZUVANJE'
    end
    object tblPocinatiOdrzuvanjeNaziv: TFIBStringField
      DisplayLabel = #1059#1089#1083#1091#1075#1072' '#1086#1076#1088#1078#1091#1074#1072#1114#1077
      FieldName = 'OdrzuvanjeNaziv'
      Size = 2
      EmptyStrToNull = True
    end
    object tblPocinatiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPocinatiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_UPD'
    end
    object tblPocinatiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiNAPLATA_IZNOS: TFIBBCDField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1077#1085#1086' - '#1080#1079#1085#1086#1089
      FieldName = 'NAPLATA_IZNOS'
      DisplayFormat = '0.00 , .'
      Size = 2
      RoundByScale = True
    end
    object tblPocinatiADRESA_ULICA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1091#1083#1080#1094#1072' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
      FieldName = 'ADRESA_ULICA'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiADRESA_BROJ: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1073#1088#1086#1112' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
      FieldName = 'ADRESA_BROJ'
      Size = 10
      EmptyStrToNull = True
    end
    object tblPocinatiADRESA_MESTO: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' - '#1084#1077#1089#1090#1086' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
      FieldName = 'ADRESA_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
      FieldName = 'DATUM_DO'
    end
    object tblPocinatiPREZIMEIMESTARATEL: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083
      FieldName = 'PREZIMEIMESTARATEL'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatiNAZIVLOKACIJA: TFIBStringField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072
      FieldName = 'NAZIVLOKACIJA'
      Size = 99
      EmptyStrToNull = True
    end
    object tblPocinatiSTARATEL_SIFRA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'STARATEL_SIFRA'
      EmptyStrToNull = True
    end
  end
  object tblStaratelLokacija: TpFIBDataSet
    SelectSQL.Strings = (
      'select gs.id,'
      '       gs.prezime_ime,'
      '       gs.adresa_ulica,'
      '       gs.adresa_broj,'
      '       gs.adresa_mesto,'
      '       gs.staratel_sifra,'
      '       gs.datum_do,'
      '       gs.ts_ins,'
      '       gs.ts_upd,'
      '       gs.usr_ins,'
      '       gs.usr_upd'
      'from gis_staratel gs'
      'right outer join gis_lokacija gl on gl.staratel_id = gs.id'
      'where gl.id = :mas_id and gs.datum_do is null'
      'order by gs.prezime_ime')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsLokacija
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 240
    Top = 40
    object tblStaratelLokacijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStaratelLokacijaPREZIME_IME: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083
      FieldName = 'PREZIME_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelLokacijaADRESA_ULICA: TFIBStringField
      DisplayLabel = #1059#1083#1080#1094#1072
      FieldName = 'ADRESA_ULICA'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelLokacijaADRESA_BROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'ADRESA_BROJ'
      Size = 10
      EmptyStrToNull = True
    end
    object tblStaratelLokacijaADRESA_MESTO: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'ADRESA_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblStaratelLokacijaDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' - '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblStaratelLokacijaSTARATEL_SIFRA: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1072#1090#1077#1083' - '#1096#1080#1092#1088#1072
      FieldName = 'STARATEL_SIFRA'
      EmptyStrToNull = True
    end
  end
  object dsStaratelLokacija: TDataSource
    DataSet = tblStaratelLokacija
    Left = 328
    Top = 40
  end
  object tblPocinatlokacija: TpFIBDataSet
    SelectSQL.Strings = (
      'select gp.id,'
      '       gp.prezime_ime,'
      '       gp.vozrast,'
      '       gp.datum_pocinat,'
      '       gp.datum_zakopan,'
      '       gp.datum_naplata,'
      '       gp.info_ispratnica,'
      '       gp.br_resenie_zakop,'
      '       gp.naplata_iznos,'
      '       gp.lokacija_id'
      'from gis_pocinati gp'
      'left outer join gis_lokacija gl on gl.id = gp.lokacija_id'
      'where gp.lokacija_id = :mas_id'
      'order by gp.prezime_ime')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsLokacija
    DefaultFormats.NumericDisplayFormat = '0.00 , .'
    Left = 232
    Top = 104
    object tblPocinatlokacijaID: TFIBIntegerField
      FieldName = 'ID'
      DisplayFormat = #1064#1080#1092#1088#1072
    end
    object tblPocinatlokacijaPREZIME_IME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077' '#1080' '#1080#1084#1077' '#1085#1072' '#1087#1086#1095#1080#1085#1072#1090#1080#1086#1090
      FieldName = 'PREZIME_IME'
      Size = 50
      EmptyStrToNull = True
    end
    object tblPocinatlokacijaVOZRAST: TFIBSmallIntField
      DisplayLabel = #1042#1086#1079#1088#1072#1089#1090
      FieldName = 'VOZRAST'
    end
    object tblPocinatlokacijaDATUM_POCINAT: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' - '#1082#1086#1075#1072' '#1077' '#1087#1086#1095#1080#1085#1072#1090
      FieldName = 'DATUM_POCINAT'
    end
    object tblPocinatlokacijaDATUM_ZAKOPAN: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' - '#1082#1086#1075#1072' '#1077' '#1079#1072#1082#1086#1087#1072#1085
      FieldName = 'DATUM_ZAKOPAN'
    end
    object tblPocinatlokacijaDATUM_NAPLATA: TFIBDateField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1077#1085#1086' - '#1076#1072#1090#1072
      FieldName = 'DATUM_NAPLATA'
    end
    object tblPocinatlokacijaINFO_ISPRATNICA: TFIBStringField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1077#1085#1086' - '#1087#1088#1080#1079'. '#1073#1088
      FieldName = 'INFO_ISPRATNICA'
      Size = 25
      EmptyStrToNull = True
    end
    object tblPocinatlokacijaBR_RESENIE_ZAKOP: TFIBStringField
      DisplayLabel = #1056#1077#1096'. '#1079#1072' '#1079#1072#1082#1086#1087
      FieldName = 'BR_RESENIE_ZAKOP'
      EmptyStrToNull = True
    end
    object tblPocinatlokacijaNAPLATA_IZNOS: TFIBBCDField
      DisplayLabel = #1053#1072#1087#1083#1072#1090#1072' - '#1080#1079#1085#1086#1089
      FieldName = 'NAPLATA_IZNOS'
      Size = 2
      RoundByScale = True
    end
    object tblPocinatlokacijaLOKACIJA_ID: TFIBIntegerField
      DisplayLabel = #1051#1086#1082#1072#1094#1080#1112#1072' (ID)'
      FieldName = 'LOKACIJA_ID'
    end
  end
  object dspocinatLokacija: TDataSource
    DataSet = tblPocinatlokacija
    Left = 328
    Top = 104
  end
end
