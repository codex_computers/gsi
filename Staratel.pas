{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.05.2012                                }
{                                                       }
{     ������ : 1.0.4521                                }
{                                                       }
{*******************************************************}


unit Staratel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxGroupBox, cxCalendar,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmStaratel = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    ulica: TcxDBTextEdit;
    Label5: TLabel;
    mesto: TcxDBTextEdit;
    broj: TcxDBTextEdit;
    Label4: TLabel;
    Label3: TLabel;
    PrezimeIme: TcxDBTextEdit;
    Label2: TLabel;
    Label7: TLabel;
    datumdo: TcxDBDateEdit;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    STARATEL_SIFRA: TcxDBTextEdit;
    Label6: TLabel;
    cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure prefrli;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStaratel: TfrmStaratel;

implementation

uses dmUnit;

{$R *.dfm}

procedure TfrmStaratel.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
     prefrli;
end;

procedure TfrmStaratel.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
    if(Ord(Key) = VK_RETURN) then
        prefrli;
end;

procedure TfrmStaratel.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblStaratel.open;
  dm.tblLokacija.Open;
end;

procedure TfrmStaratel.prefrli;
begin
    inherited;
    ModalResult := mrOk;
    SetSifra(0,IntToStr(dm.tblStaratelID.Value));
end;

end.
