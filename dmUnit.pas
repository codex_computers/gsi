
{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.05.2012                                }
{                                                       }
{     ������ : 1.0.4521                                }
{                                                       }
{*******************************************************}


unit dmUnit;

interface

uses
  SysUtils, Classes, ImgList, DB, Controls, cxGraphics,FIBDataSet, pFIBDataSet;

type
  Tdm = class(TDataModule)
    dsLokacija: TDataSource;
    tblLokacija: TpFIBDataSet;
    tblLokacijaID: TFIBIntegerField;
    tblLokacijaBLOK: TFIBStringField;
    tblLokacijaRED: TFIBStringField;
    tblLokacijaGROB: TFIBStringField;
    tblLokacijaODRZUVANJE: TFIBSmallIntField;
    tblLokacijaTS_INS: TFIBDateTimeField;
    tblLokacijaTS_UPD: TFIBDateTimeField;
    tblLokacijaUSR_INS: TFIBStringField;
    tblLokacijaUSR_UPD: TFIBStringField;
    tblLokacijaOdrzuvanjeNaziv: TFIBStringField;
    dsStaratel: TDataSource;
    tblStaratel: TpFIBDataSet;
    tblStaratelID: TFIBIntegerField;
    tblStaratelPREZIME_IME: TFIBStringField;
    tblStaratelADRESA_ULICA: TFIBStringField;
    tblStaratelADRESA_BROJ: TFIBStringField;
    tblStaratelADRESA_MESTO: TFIBStringField;
    tblStaratelTS_INS: TFIBDateTimeField;
    tblStaratelTS_UPD: TFIBDateTimeField;
    tblStaratelUSR_INS: TFIBStringField;
    tblStaratelUSR_UPD: TFIBStringField;
    dsPocinati: TDataSource;
    tblPocinati: TpFIBDataSet;
    tblPocinatiID: TFIBIntegerField;
    tblPocinatiPREZIME_IME: TFIBStringField;
    tblPocinatiVOZRAST: TFIBSmallIntField;
    tblPocinatiDATUM_POCINAT: TFIBDateField;
    tblPocinatiDATUM_ZAKOPAN: TFIBDateField;
    tblPocinatiDATUM_NAPLATA: TFIBDateField;
    tblPocinatiINFO_ISPRATNICA: TFIBStringField;
    tblPocinatiBR_RESENIE_ZAKOP: TFIBStringField;
    tblPocinatiLOKACIJA_ID: TFIBIntegerField;
    tblPocinatiBLOK: TFIBStringField;
    tblPocinatiRED: TFIBStringField;
    tblPocinatiGROB: TFIBStringField;
    tblPocinatiODRZUVANJE: TFIBSmallIntField;
    tblPocinatiOdrzuvanjeNaziv: TFIBStringField;
    tblPocinatiTS_INS: TFIBDateTimeField;
    tblPocinatiTS_UPD: TFIBDateTimeField;
    tblPocinatiUSR_INS: TFIBStringField;
    tblPocinatiUSR_UPD: TFIBStringField;
    tblLokacijaNAZIVLOKACIJA: TFIBStringField;
    tblPocinatiNAPLATA_IZNOS: TFIBBCDField;
    tblStaratelDATUM_DO: TFIBDateField;
    tblStaratelLokacija: TpFIBDataSet;
    tblStaratelLokacijaID: TFIBIntegerField;
    tblStaratelLokacijaPREZIME_IME: TFIBStringField;
    tblStaratelLokacijaADRESA_ULICA: TFIBStringField;
    tblStaratelLokacijaADRESA_BROJ: TFIBStringField;
    tblStaratelLokacijaADRESA_MESTO: TFIBStringField;
    tblStaratelLokacijaDATUM_DO: TFIBDateField;
    dsStaratelLokacija: TDataSource;
    tblPocinatlokacija: TpFIBDataSet;
    dspocinatLokacija: TDataSource;
    tblPocinatlokacijaID: TFIBIntegerField;
    tblPocinatlokacijaPREZIME_IME: TFIBStringField;
    tblPocinatlokacijaVOZRAST: TFIBSmallIntField;
    tblPocinatlokacijaDATUM_POCINAT: TFIBDateField;
    tblPocinatlokacijaDATUM_ZAKOPAN: TFIBDateField;
    tblPocinatlokacijaDATUM_NAPLATA: TFIBDateField;
    tblPocinatlokacijaINFO_ISPRATNICA: TFIBStringField;
    tblPocinatlokacijaBR_RESENIE_ZAKOP: TFIBStringField;
    tblPocinatlokacijaNAPLATA_IZNOS: TFIBBCDField;
    tblPocinatlokacijaLOKACIJA_ID: TFIBIntegerField;
    tblLokacijaSTARATEL_ID: TFIBIntegerField;
    tblLokacijaPREZIME_IME: TFIBStringField;
    tblLokacijaADRESA_ULICA: TFIBStringField;
    tblLokacijaADRESA_BROJ: TFIBStringField;
    tblLokacijaADRESA_MESTO: TFIBStringField;
    tblLokacijaDATUM_DO: TFIBDateField;
    tblPocinatiADRESA_ULICA: TFIBStringField;
    tblPocinatiADRESA_BROJ: TFIBStringField;
    tblPocinatiADRESA_MESTO: TFIBStringField;
    tblPocinatiDATUM_DO: TFIBDateField;
    tblPocinatiPREZIMEIMESTARATEL: TFIBStringField;
    tblPocinatiNAZIVLOKACIJA: TFIBStringField;
    tblStaratelSTARATEL_SIFRA: TFIBStringField;
    tblPocinatiSTARATEL_SIFRA: TFIBStringField;
    tblLokacijaSTARATEL_SIFRA: TFIBStringField;
    tblStaratelLokacijaSTARATEL_SIFRA: TFIBStringField;
    procedure TabelaBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;
  firma:Integer;
implementation

uses dmKonekcija, DaNe;

{$R *.dfm}

procedure Tdm.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

end.
