{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.05.2012                                }
{                                                       }
{     ������ : 1.0.4521                                }
{                                                       }
{*******************************************************}

unit Lokacija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxGroupBox, cxRadioGroup,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint;

type
  TfrmLokacija = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BLOK: TcxGridDBColumn;
    cxGrid1DBTableView1RED: TcxGridDBColumn;
    cxGrid1DBTableView1GROB: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    Blok: TcxDBTextEdit;
    Label3: TLabel;
    Red: TcxDBTextEdit;
    Label4: TLabel;
    Grob: TcxDBTextEdit;
    cxDBRadioOdrzuvanje: TcxDBRadioGroup;
    cxGrid1DBTableView1OdrzuvanjeNaziv: TcxGridDBColumn;
    txtStaratel: TcxDBTextEdit;
    cbStaratel: TcxDBLookupComboBox;
    Label6: TLabel;
    cxGrid1DBTableView1NAZIVLOKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1STARATEL_ID: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure prefrli;
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLokacija: TfrmLokacija;

implementation

uses dmUnit, Staratel;

{$R *.dfm}

procedure TfrmLokacija.prefrli;
begin
    inherited;
    ModalResult := mrOk;
    SetSifra(0,IntToStr(dm.tblLokacijaID.Value));
end;

procedure TfrmLokacija.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;
  prefrli;
end;

procedure TfrmLokacija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
      if(Ord(Key) = VK_RETURN) then
        prefrli;
end;

procedure TfrmLokacija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      inherited;
      if (Sender = txtStaratel) or (Sender = cbStaratel) then
         begin
            case Key of
              VK_INSERT:
                begin
                  frmStaratel:=TfrmStaratel.Create(self,false);
                  frmStaratel.ShowModal;
                  if (frmStaratel.ModalResult = mrOK) then
                    begin
                      dm.tblLokacijaSTARATEL_ID.Value := StrToInt(frmStaratel.GetSifra(0));
                    end;
                  frmStaratel.Free;
                end;
            end;
         end;
end;

procedure TfrmLokacija.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblLokacija.Open;
  dm.tblStaratel.open;
end;

end.
