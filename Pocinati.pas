
{*******************************************************}
{                                                       }
{     ��������� : ������ �������                       }
{                                                       }
{     ����� : 17.05.2012                                }
{                                                       }
{     ������ : 1.0.4521                                }
{                                                       }
{*******************************************************}

unit Pocinati;
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, ActnList, cxBarEditItem, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon, StdCtrls, cxButtons,
  cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, cxGroupBox, cxMaskEdit,
  cxCalendar, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint;

type
  TfrmPocinati = class(TfrmMaster)
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn;
    cxGrid1DBTableView1VOZRAST: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_POCINAT: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_ZAKOPAN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_NAPLATA: TcxGridDBColumn;
    cxGrid1DBTableView1INFO_ISPRATNICA: TcxGridDBColumn;
    cxGrid1DBTableView1BR_RESENIE_ZAKOP: TcxGridDBColumn;
    cxGrid1DBTableView1LOKACIJA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1BLOK: TcxGridDBColumn;
    cxGrid1DBTableView1RED: TcxGridDBColumn;
    cxGrid1DBTableView1GROB: TcxGridDBColumn;
    cxGrid1DBTableView1OdrzuvanjeNaziv: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1NAPLATA_IZNOS: TcxGridDBColumn;
    gbPocinat: TcxGroupBox;
    Label3: TLabel;
    Label2: TLabel;
    vozrast: TcxDBTextEdit;
    PrezimeIme: TcxDBTextEdit;
    gbData: TcxGroupBox;
    Label7: TLabel;
    DATUM_POCINAT: TcxDBDateEdit;
    Label4: TLabel;
    DATUM_ZAKOPAN: TcxDBDateEdit;
    gbNaplateno: TcxGroupBox;
    Label5: TLabel;
    INFO_ISPRATNICA: TcxDBTextEdit;
    Label6: TLabel;
    DATUM_NAPLATA: TcxDBDateEdit;
    Label8: TLabel;
    NAPLATA_IZNOS: TcxDBTextEdit;
    Label9: TLabel;
    BR_RESENIE_ZAKOP: TcxDBTextEdit;
    cbLokacija: TcxDBLookupComboBox;
    txtlokacija: TcxDBTextEdit;
    Label10: TLabel;
    cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIMEIMESTARATEL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIVLOKACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPocinati: TfrmPocinati;

implementation

uses dmUnit, Lokacija;

{$R *.dfm}

procedure TfrmPocinati.aAzurirajExecute(Sender: TObject);
begin
  inherited;
   PrezimeIme.SetFocus;
end;

procedure TfrmPocinati.aNovExecute(Sender: TObject);
begin
  inherited;
  PrezimeIme.SetFocus;
end;

procedure TfrmPocinati.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Sender = txtlokacija) or (Sender = cbLokacija) then
         begin
            case Key of
              VK_INSERT:
                begin
                  frmLokacija:=TfrmLokacija.Create(self,false);
                  frmLokacija.ShowModal;
                  if (frmLokacija.ModalResult = mrOK) then
                    begin
                      dm.tblPocinatiLOKACIJA_ID.Value := StrToInt(frmLokacija.GetSifra(0));
                    end;
                  frmLokacija.Free;
                end;
            end;
         end;
end;

procedure TfrmPocinati.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblLokacija.Open;
  dm.tblPocinati.Open;
end;

end.
