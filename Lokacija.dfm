inherited frmLokacija: TfrmLokacija
  Caption = #1051#1086#1082#1072#1094#1080#1080
  ClientHeight = 590
  ClientWidth = 695
  ExplicitWidth = 711
  ExplicitHeight = 628
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 695
    ExplicitWidth = 695
    inherited cxGrid1: TcxGrid
      Width = 691
      ExplicitWidth = 691
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dm.dsLokacija
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072#1090#1077' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1085#1086' '#1087#1086#1076#1072#1090#1086#1094#1080
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1BLOK: TcxGridDBColumn
          DataBinding.FieldName = 'BLOK'
          Width = 58
        end
        object cxGrid1DBTableView1RED: TcxGridDBColumn
          DataBinding.FieldName = 'RED'
          Width = 58
        end
        object cxGrid1DBTableView1GROB: TcxGridDBColumn
          DataBinding.FieldName = 'GROB'
          Width = 58
        end
        object cxGrid1DBTableView1NAZIVLOKACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIVLOKACIJA'
          Width = 153
        end
        object cxGrid1DBTableView1STARATEL_ID: TcxGridDBColumn
          DataBinding.FieldName = 'STARATEL_ID'
          Visible = False
          Width = 101
        end
        object cxGrid1DBTableView1OdrzuvanjeNaziv: TcxGridDBColumn
          DataBinding.FieldName = 'OdrzuvanjeNaziv'
          Width = 118
        end
        object cxGrid1DBTableView1PREZIME_IME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME_IME'
          Width = 173
        end
        object cxGrid1DBTableView1STARATEL_SIFRA: TcxGridDBColumn
          DataBinding.FieldName = 'STARATEL_SIFRA'
          Width = 104
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1076#1086' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
          DataBinding.FieldName = 'DATUM_DO'
          Visible = False
          Width = 57
        end
        object cxGrid1DBTableView1ADRESA_ULICA: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072' - '#1091#1083#1080#1094#1072' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
          DataBinding.FieldName = 'ADRESA_ULICA'
          Visible = False
        end
        object cxGrid1DBTableView1ADRESA_BROJ: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072' - '#1073#1088#1086#1112' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
          DataBinding.FieldName = 'ADRESA_BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1ADRESA_MESTO: TcxGridDBColumn
          Caption = #1040#1076#1088#1077#1089#1072' - '#1084#1077#1089#1090#1086' ('#1085#1072' '#1089#1090#1072#1088#1072#1090#1077#1083')'
          DataBinding.FieldName = 'ADRESA_MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 695
    Height = 191
    ExplicitWidth = 695
    ExplicitHeight = 191
    inherited Label1: TLabel
      Left = 442
      Top = 9
      Visible = False
      ExplicitLeft = 442
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 45
      Top = 25
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1083#1086#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 45
      Top = 52
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1077#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 45
      Top = 79
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1043#1088#1086#1073' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel [4]
      Left = 16
      Top = 117
      Width = 79
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1090#1072#1088#1072#1090#1077#1083' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 498
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsLokacija
      TabOrder = 3
      Visible = False
      ExplicitLeft = 498
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 604
      Top = 151
      TabOrder = 8
      ExplicitLeft = 604
      ExplicitTop = 151
    end
    inherited ZapisiButton: TcxButton
      Left = 523
      Top = 151
      TabOrder = 7
      ExplicitLeft = 523
      ExplicitTop = 151
    end
    object Blok: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 22
      BeepOnEnter = False
      DataBinding.DataField = 'BLOK'
      DataBinding.DataSource = dm.dsLokacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object Red: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 49
      BeepOnEnter = False
      DataBinding.DataField = 'RED'
      DataBinding.DataSource = dm.dsLokacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object Grob: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 76
      BeepOnEnter = False
      DataBinding.DataField = 'GROB'
      DataBinding.DataSource = dm.dsLokacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxDBRadioOdrzuvanje: TcxDBRadioGroup
      Left = 272
      Top = 30
      TabStop = False
      Caption = #1059#1089#1083#1091#1075#1072' '#1086#1076#1088#1078#1091#1074#1072#1114#1077
      DataBinding.DataField = 'ODRZUVANJE'
      DataBinding.DataSource = dm.dsLokacija
      Properties.Items = <
        item
          Caption = #1044#1072
          Value = '1'
        end
        item
          Caption = #1053#1077
          Value = '0'
        end>
      TabOrder = 4
      Height = 59
      Width = 121
    end
    object txtStaratel: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 114
      BeepOnEnter = False
      DataBinding.DataField = 'STARATEL_ID'
      DataBinding.DataSource = dm.dsLokacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 50
    end
    object cbStaratel: TcxDBLookupComboBox
      Tag = 1
      Left = 153
      Top = 114
      DataBinding.DataField = 'STARATEL_ID'
      DataBinding.DataSource = dm.dsLokacija
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Caption = #1057#1090#1072#1088#1072#1090#1077#1083
          FieldName = 'PREZIME_IME'
        end
        item
          FieldName = 'STARATEL_SIFRA'
        end>
      Properties.ListSource = dm.dsStaratel
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 240
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 695
    ExplicitWidth = 695
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 567
    Width = 695
    ExplicitTop = 567
    ExplicitWidth = 695
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 256
  end
  inherited PopupMenu1: TPopupMenu
    Left = 352
    Top = 224
  end
  inherited dxBarManager1: TdxBarManager
    Left = 424
    Top = 224
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 102
      FloatClientHeight = 76
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 144
    Top = 232
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.ScaleMode = smFit
      ReportDocument.CreationDate = 40820.460184548610000000
      ShrinkToPageWidth = True
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
