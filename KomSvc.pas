// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://10.20.1.55:86/KomSvc.asmx?WSDL
//  >Import : http://10.20.1.55:86/KomSvc.asmx?WSDL>0
// Encoding : utf-8
// Version  : 1.0
// (18.10.2011 13:47:50 - - $Rev: 19514 $)
// ************************************************************************ //

unit KomSvc;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:int             - "http://www.w3.org/2001/XMLSchema"[Gbl]


  string_         =  type string;      { "http://tempuri.org/"[GblElm] }

  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // soapAction: http://tempuri.org/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : KomSvcSoap
  // service   : KomSvc
  // port      : KomSvcSoap
  // URL       : http://10.20.1.55:86/KomSvc.asmx
  // ************************************************************************ //
  KomSvcSoap = interface(IInvokable)
  ['{20E655EA-760E-894D-CC09-4AE781180671}']
    function  GET_PARTNER_FROM_FAK(const FAKTURA: string; const LOKACIJA: Integer): string; stdcall;
    function  GET_PARTNER_INFO(const TP: Integer; const P: Integer): string; stdcall;
    function  UPDATE_PARTNER_INFO(const TP: Integer; const P: Integer; const IME: string; const PREZIME: string; const ULICA: string; const BROJ: string; 
                                  const MESTO: string; const EMBG: string; const FLAG: Integer): string; stdcall;
    function  GET_KARTICA_PARTNER(const TP: Integer; const P: Integer; const F: Integer): string; stdcall;
  end;


  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // binding   : KomSvcHttpGet
  // service   : KomSvc
  // port      : KomSvcHttpGet
  // ************************************************************************ //
  KomSvcHttpGet = interface(IInvokable)
  ['{D9F651E8-7572-300B-FF35-D823751D96D5}']
    function  GET_PARTNER_FROM_FAK(const FAKTURA: string; const LOKACIJA: string): string_; stdcall;
    function  GET_PARTNER_INFO(const TP: string; const P: string): string_; stdcall;
    function  UPDATE_PARTNER_INFO(const TP: string; const P: string; const IME: string; const PREZIME: string; const ULICA: string; const BROJ: string; 
                                  const MESTO: string; const EMBG: string; const FLAG: string): string_; stdcall;
    function  GET_KARTICA_PARTNER(const TP: string; const P: string; const F: string): string_; stdcall;
  end;


  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // binding   : KomSvcHttpPost
  // service   : KomSvc
  // port      : KomSvcHttpPost
  // ************************************************************************ //
  KomSvcHttpPost = interface(IInvokable)
  ['{EF8774A6-4A13-328E-5D97-D5A28A06B04A}']
    function  GET_PARTNER_FROM_FAK(const FAKTURA: string; const LOKACIJA: string): string_; stdcall;
    function  GET_PARTNER_INFO(const TP: string; const P: string): string_; stdcall;
    function  UPDATE_PARTNER_INFO(const TP: string; const P: string; const IME: string; const PREZIME: string; const ULICA: string; const BROJ: string; 
                                  const MESTO: string; const EMBG: string; const FLAG: string): string_; stdcall;
    function  GET_KARTICA_PARTNER(const TP: string; const P: string; const F: string): string_; stdcall;
  end;

function GetKomSvcSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): KomSvcSoap;
function GetKomSvcHttpGet(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): KomSvcHttpGet;
function GetKomSvcHttpPost(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): KomSvcHttpPost;


implementation
  uses SysUtils;

function GetKomSvcSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): KomSvcSoap;
const
  defWSDL = 'http://10.20.1.55:86/KomSvc.asmx?WSDL';
  defURL  = 'http://10.20.1.55:86/KomSvc.asmx';
  defSvc  = 'KomSvc';
  defPrt  = 'KomSvcSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as KomSvcSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


function GetKomSvcHttpGet(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): KomSvcHttpGet;
const
  defWSDL = 'http://10.20.1.55:86/KomSvc.asmx?WSDL';
  defURL  = '';
  defSvc  = 'KomSvc';
  defPrt  = 'KomSvcHttpGet';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as KomSvcHttpGet);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


function GetKomSvcHttpPost(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): KomSvcHttpPost;
const
  defWSDL = 'http://10.20.1.55:86/KomSvc.asmx?WSDL';
  defURL  = '';
  defSvc  = 'KomSvc';
  defPrt  = 'KomSvcHttpPost';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as KomSvcHttpPost);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(KomSvcSoap), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(KomSvcSoap), 'http://tempuri.org/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(KomSvcSoap), ioDocument);
  InvRegistry.RegisterInterface(TypeInfo(KomSvcHttpGet), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(KomSvcHttpGet), '');
  InvRegistry.RegisterInterface(TypeInfo(KomSvcHttpPost), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(KomSvcHttpPost), '');
  RemClassRegistry.RegisterXSInfo(TypeInfo(string_), 'http://tempuri.org/', 'string_', 'string');

end.