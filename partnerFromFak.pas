
{*********************************************************}
{                                                         }
{                    XML Data Binding                     }
{                                                         }
{         Generated on: 18.10.2011 15:19:01               }
{       Generated from: D:\Codis\GSI\partnerFromFak.xml   }
{   Settings stored in: D:\Codis\GSI\partnerFromFak.xdb   }
{                                                         }
{*********************************************************}

unit partnerFromFak;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRootType = interface;
  IXMLPARTNERType = interface;

{ IXMLRootType }

  IXMLRootType = interface(IXMLNode)
    ['{89C21DC7-7E19-4547-9DAB-8FC861ABEA98}']
    { Property Accessors }
    function Get_PARTNER: IXMLPARTNERType;
    { Methods & Properties }
    property PARTNER: IXMLPARTNERType read Get_PARTNER;
  end;

{ IXMLPARTNERType }

  IXMLPARTNERType = interface(IXMLNode)
    ['{2C8866C0-BE38-4DAB-A61D-3C6F3F075E99}']
    { Property Accessors }
    function Get_FLAG: Integer;
    procedure Set_FLAG(Value: Integer);
    { Methods & Properties }
    property FLAG: Integer read Get_FLAG write Set_FLAG;
  end;

{ Forward Decls }

  TXMLRootType = class;
  TXMLPARTNERType = class;

{ TXMLRootType }

  TXMLRootType = class(TXMLNode, IXMLRootType)
  protected
    { IXMLRootType }
    function Get_PARTNER: IXMLPARTNERType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLPARTNERType }

  TXMLPARTNERType = class(TXMLNode, IXMLPARTNERType)
  protected
    { IXMLPARTNERType }
    function Get_FLAG: Integer;
    procedure Set_FLAG(Value: Integer);
  end;

{ Global Functions }

function GetRoot(Doc: IXMLDocument): IXMLRootType;
function LoadRoot(const FileName: WideString): IXMLRootType;
function NewRoot: IXMLRootType;

function GetPartner(Doc: IXMLDocument): IXMLPARTNERType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRoot(Doc: IXMLDocument): IXMLRootType;
begin
  Result := Doc.GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

function LoadRoot(const FileName: WideString): IXMLRootType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

function NewRoot: IXMLRootType;
begin
  Result := NewXMLDocument.GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

function GetPartner(Doc: IXMLDocument): IXMLPARTNERType;
begin
  Result := Doc.GetDocBinding('Partner', TXMLPARTNERType, TargetNamespace) as IXMLPARTNERType;
end;

{ TXMLRootType }

procedure TXMLRootType.AfterConstruction;
begin
  RegisterChildNode('PARTNER', TXMLPARTNERType);
  inherited;
end;

function TXMLRootType.Get_PARTNER: IXMLPARTNERType;
begin
  Result := ChildNodes['PARTNER'] as IXMLPARTNERType;
end;

{ TXMLPARTNERType }

function TXMLPARTNERType.Get_FLAG: Integer;
begin
  Result := ChildNodes['FLAG'].NodeValue;
end;

procedure TXMLPARTNERType.Set_FLAG(Value: Integer);
begin
  ChildNodes['FLAG'].NodeValue := Value;
end;

end.