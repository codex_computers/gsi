
{*********************************************************}
{                                                         }
{                    XML Data Binding                     }
{                                                         }
{         Generated on: 19.10.2011 12:06:08               }
{       Generated from: D:\Codis\GSI\karticaPartner.xml   }
{   Settings stored in: D:\Codis\GSI\karticaPartner.xdb   }
{                                                         }
{*********************************************************}

unit karticaPartner;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLRootType = interface;
  IXMLKARTICAPARTNERType = interface;

{ IXMLRootType }

  IXMLRootType = interface(IXMLNodeCollection)
    ['{3B0F1ED6-3FFF-4732-B409-42FF3514B0BF}']
    { Property Accessors }
    function Get_KARTICAPARTNER(Index: Integer): IXMLKARTICAPARTNERType;
    { Methods & Properties }
    function Add: IXMLKARTICAPARTNERType;
    function Insert(const Index: Integer): IXMLKARTICAPARTNERType;
    property KARTICAPARTNER[Index: Integer]: IXMLKARTICAPARTNERType read Get_KARTICAPARTNER; default;
  end;

{ IXMLKARTICAPARTNERType }

  IXMLKARTICAPARTNERType = interface(IXMLNode)
    ['{43456716-795C-4784-9113-2D6315C81A50}']
    { Property Accessors }
    function Get_LOKACIJA_ID: Integer;
    function Get_BROJ_FAKTURA: WideString;
    function Get_DATUM_PRESMETKA: WideString;
    function Get_DOLZI: Integer;
    function Get_POBARUVA: Integer;
    function Get_UPLATENO: WideString;
    function Get_SALDO: Integer;
    function Get_MESEC: Integer;
    function Get_GODINA: Integer;
    procedure Set_LOKACIJA_ID(Value: Integer);
    procedure Set_BROJ_FAKTURA(Value: WideString);
    procedure Set_DATUM_PRESMETKA(Value: WideString);
    procedure Set_DOLZI(Value: Integer);
    procedure Set_POBARUVA(Value: Integer);
    procedure Set_UPLATENO(Value: WideString);
    procedure Set_SALDO(Value: Integer);
    procedure Set_MESEC(Value: Integer);
    procedure Set_GODINA(Value: Integer);
    { Methods & Properties }
    property LOKACIJA_ID: Integer read Get_LOKACIJA_ID write Set_LOKACIJA_ID;
    property BROJ_FAKTURA: WideString read Get_BROJ_FAKTURA write Set_BROJ_FAKTURA;
    property DATUM_PRESMETKA: WideString read Get_DATUM_PRESMETKA write Set_DATUM_PRESMETKA;
    property DOLZI: Integer read Get_DOLZI write Set_DOLZI;
    property POBARUVA: Integer read Get_POBARUVA write Set_POBARUVA;
    property UPLATENO: WideString read Get_UPLATENO write Set_UPLATENO;
    property SALDO: Integer read Get_SALDO write Set_SALDO;
    property MESEC: Integer read Get_MESEC write Set_MESEC;
    property GODINA: Integer read Get_GODINA write Set_GODINA;
  end;

{ Forward Decls }

  TXMLRootType = class;
  TXMLKARTICAPARTNERType = class;

{ TXMLRootType }

  TXMLRootType = class(TXMLNodeCollection, IXMLRootType)
  protected
    { IXMLRootType }
    function Get_KARTICAPARTNER(Index: Integer): IXMLKARTICAPARTNERType;
    function Add: IXMLKARTICAPARTNERType;
    function Insert(const Index: Integer): IXMLKARTICAPARTNERType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLKARTICAPARTNERType }

  TXMLKARTICAPARTNERType = class(TXMLNode, IXMLKARTICAPARTNERType)
  protected
    { IXMLKARTICAPARTNERType }
    function Get_LOKACIJA_ID: Integer;
    function Get_BROJ_FAKTURA: WideString;
    function Get_DATUM_PRESMETKA: WideString;
    function Get_DOLZI: Integer;
    function Get_POBARUVA: Integer;
    function Get_UPLATENO: WideString;
    function Get_SALDO: Integer;
    function Get_MESEC: Integer;
    function Get_GODINA: Integer;
    procedure Set_LOKACIJA_ID(Value: Integer);
    procedure Set_BROJ_FAKTURA(Value: WideString);
    procedure Set_DATUM_PRESMETKA(Value: WideString);
    procedure Set_DOLZI(Value: Integer);
    procedure Set_POBARUVA(Value: Integer);
    procedure Set_UPLATENO(Value: WideString);
    procedure Set_SALDO(Value: Integer);
    procedure Set_MESEC(Value: Integer);
    procedure Set_GODINA(Value: Integer);
  end;

{ Global Functions }

function GetRoot(Doc: IXMLDocument): IXMLRootType;
function LoadRoot(const FileName: WideString): IXMLRootType;
function NewRoot: IXMLRootType;

const
  TargetNamespace = '';

implementation

{ Global Functions }

function GetRoot(Doc: IXMLDocument): IXMLRootType;
begin
  Result := Doc.GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

function LoadRoot(const FileName: WideString): IXMLRootType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

function NewRoot: IXMLRootType;
begin
  Result := NewXMLDocument.GetDocBinding('Root', TXMLRootType, TargetNamespace) as IXMLRootType;
end;

{ TXMLRootType }

procedure TXMLRootType.AfterConstruction;
begin
  RegisterChildNode('KARTICAPARTNER', TXMLKARTICAPARTNERType);
  ItemTag := 'KARTICAPARTNER';
  ItemInterface := IXMLKARTICAPARTNERType;
  inherited;
end;

function TXMLRootType.Get_KARTICAPARTNER(Index: Integer): IXMLKARTICAPARTNERType;
begin
  Result := List[Index] as IXMLKARTICAPARTNERType;
end;

function TXMLRootType.Add: IXMLKARTICAPARTNERType;
begin
  Result := AddItem(-1) as IXMLKARTICAPARTNERType;
end;

function TXMLRootType.Insert(const Index: Integer): IXMLKARTICAPARTNERType;
begin
  Result := AddItem(Index) as IXMLKARTICAPARTNERType;
end;

{ TXMLKARTICAPARTNERType }

function TXMLKARTICAPARTNERType.Get_LOKACIJA_ID: Integer;
begin
  Result := ChildNodes['LOKACIJA_ID'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_LOKACIJA_ID(Value: Integer);
begin
  ChildNodes['LOKACIJA_ID'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_BROJ_FAKTURA: WideString;
begin
  Result := ChildNodes['BROJ_FAKTURA'].Text;
end;

procedure TXMLKARTICAPARTNERType.Set_BROJ_FAKTURA(Value: WideString);
begin
  ChildNodes['BROJ_FAKTURA'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_DATUM_PRESMETKA: WideString;
begin
  Result := ChildNodes['DATUM_PRESMETKA'].Text;
end;

procedure TXMLKARTICAPARTNERType.Set_DATUM_PRESMETKA(Value: WideString);
begin
  ChildNodes['DATUM_PRESMETKA'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_DOLZI: Integer;
begin
  Result := ChildNodes['DOLZI'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_DOLZI(Value: Integer);
begin
  ChildNodes['DOLZI'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_POBARUVA: Integer;
begin
  Result := ChildNodes['POBARUVA'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_POBARUVA(Value: Integer);
begin
  ChildNodes['POBARUVA'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_UPLATENO: WideString;
begin
  Result := ChildNodes['UPLATENO'].Text;
end;

procedure TXMLKARTICAPARTNERType.Set_UPLATENO(Value: WideString);
begin
  ChildNodes['UPLATENO'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_SALDO: Integer;
begin
  Result := ChildNodes['SALDO'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_SALDO(Value: Integer);
begin
  ChildNodes['SALDO'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_MESEC: Integer;
begin
  Result := ChildNodes['MESEC'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_MESEC(Value: Integer);
begin
  ChildNodes['MESEC'].NodeValue := Value;
end;

function TXMLKARTICAPARTNERType.Get_GODINA: Integer;
begin
  Result := ChildNodes['GODINA'].NodeValue;
end;

procedure TXMLKARTICAPARTNERType.Set_GODINA(Value: Integer);
begin
  ChildNodes['GODINA'].NodeValue := Value;
end;

end.